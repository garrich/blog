# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Coursera.org Web Application Architectures course homeworks

### How do I get set up? ###

 * Installing ROR:
```
sudo apt-get install curl

curl -L get.rvm.io | bash -s stable

source ~/.rvm/scripts/rvm

rvm requirements

rvm install ruby

ruby -v

gem install rails

rails -v
```
 * Installing node.js
```
sudo apt-get install nodejs
```
 * Installing sqliteman
``` 
sudo apt-get install sqliteman
```
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions